﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace Monoid.Unity.Material {
  [RequireComponent (typeof (Renderer))]
  public class PerRendererProperties : MonoBehaviour {

    #region -Property-

    [Serializable]
    internal struct Property {
      public enum Type {
        Color,
        Vector,
        Float,
        Range,
        Texture
      }

#pragma warning disable 0649

      public string name, label;
      public Type type;
      public Color color;
      public Vector4 vector;
      public Texture texture;
      public bool scaleOffset;

#pragma warning restore 0649

      public void Set (MaterialPropertyBlock mpb) {
        switch (type) {
          case Type.Color:
            mpb.SetColor (name, color);
            break;
          case Type.Float:
          case Type.Range:
            mpb.SetFloat (name, vector.x);
            break;
          case Type.Vector:
            mpb.SetVector (name, vector);
            break;
          case Type.Texture:
            if (texture) {
              mpb.SetTexture (name, texture);
            }
            if (scaleOffset) {
              mpb.SetVector (name + "_ST", vector);
            }
            break;
        }
      }
    }

    #endregion

    private static MaterialPropertyBlock mpb; // static because MonoBehaviors run on the Unity Main Thread

    [SerializeField]
    internal Property[] properties = { };

    void Awake () {
      Set ();
#if !UNITY_EDITOR
      Destroy (this);
#endif
    }

    internal void Set () {
      var renderer = GetComponent<Renderer> ();
      if (renderer) {
        if (mpb == null) {
          mpb = new MaterialPropertyBlock ();
        }
        mpb.Clear ();
        renderer.GetPropertyBlock (mpb);
        foreach (var prop in properties) {
          prop.Set (mpb);
        }
        renderer.SetPropertyBlock (mpb);
      }
    }

#if UNITY_EDITOR
    void OnValidate () => SetDirty ();

    internal void SetDirty () {
      Set ();
      if (EditorApplication.isPlayingOrWillChangePlaymode) {
        return;
      }
      EditorUtility.SetDirty (this);
      if (gameObject.scene.isLoaded) {
        EditorSceneManager.MarkSceneDirty (gameObject.scene);
      }
    }

    internal void Sync () {
      var renderer = GetComponent<Renderer> ();
      if (!renderer) {
        return;
      }

      var mats = renderer.sharedMaterials;
      if (mats.Length == 0) {
        properties = new Property[0];
      }

      var list = new List<Property> ();
      var props = MaterialEditor.GetMaterialProperties (mats);
      foreach (var prop in props) {
        if (!prop.flags.HasFlag (MaterialProperty.PropFlags.PerRendererData)) {
          continue;
        }

        var property = new Property {
          name = prop.name,
          label = prop.displayName,
        };
        switch (prop.type) {
          case MaterialProperty.PropType.Texture:
            property.type = Property.Type.Texture;
            property.vector = prop.textureScaleAndOffset;
            property.scaleOffset = !prop.flags.HasFlag (MaterialProperty.PropFlags.NoScaleOffset);
            break;
          case MaterialProperty.PropType.Color:
            property.type = Property.Type.Color;
            property.color = prop.colorValue;
            break;
          case MaterialProperty.PropType.Vector:
            property.type = Property.Type.Vector;
            property.vector = prop.vectorValue;
            break;
          case MaterialProperty.PropType.Range:
            property.type = Property.Type.Range;
            property.vector.x = prop.floatValue;
            property.vector.y = prop.rangeLimits.x;
            property.vector.z = prop.rangeLimits.y;
            break;
          case MaterialProperty.PropType.Float:
            property.type = Property.Type.Float;
            property.vector.x = prop.floatValue;
            break;
        }

        list.Add (property);
      }
      properties = list.ToArray ();
      SetDirty ();
    }
#endif
  }

#if UNITY_EDITOR

  [CustomEditor (typeof (PerRendererProperties))]
  public class PerRendererPropertiesEditor : Editor {

    public override void OnInspectorGUI () {
      DrawPropertiesExcluding (serializedObject, "properties");

      var target = this.target as PerRendererProperties;
      if (!target) {
        return;
      }

      var dirty = false;

      if (GUILayout.Button ("Sync PerRendererData Properties")) {
        target.Sync ();
        dirty = true;
      }

      EditorGUILayout.Separator ();

      EditorGUI.BeginChangeCheck ();
      var properties = target.properties;
      for (int i = 0, n = properties.Length; i < n; i++) {
        var prop = properties[i];
        switch (prop.type) {
          case PerRendererProperties.Property.Type.Color:
            prop.color = EditorGUILayout.ColorField (prop.label, prop.color);
            break;
          case PerRendererProperties.Property.Type.Vector:
            prop.vector = EditorGUILayout.Vector4Field (prop.label, prop.vector);
            break;
          case PerRendererProperties.Property.Type.Float:
            prop.vector.x = EditorGUILayout.FloatField (prop.label, prop.vector.x);
            break;
          case PerRendererProperties.Property.Type.Range:
            prop.vector.x = EditorGUILayout.Slider (prop.label, prop.vector.x, prop.vector.y, prop.vector.z);
            break;
          case PerRendererProperties.Property.Type.Texture:
            prop.texture = (Texture) EditorGUILayout.ObjectField (prop.label, prop.texture, typeof (Texture), false);
            if (prop.scaleOffset) {
              EditorGUI.indentLevel++;
              prop.vector = EditorGUILayout.Vector4Field ("Scale and Offset", prop.vector);
              EditorGUI.indentLevel--;
            }
            break;
        }
        properties[i] = prop;
      }

      dirty |= EditorGUI.EndChangeCheck ();
      if (dirty) {
        target.SetDirty ();
      }
    }
  }

#endif

}