Common Material related Utilities for Unity3D
=============================================

Features include:

`PerRendererProperties`
A script which lets allows to set material properties annoted with `[PerRendererData]` conveniently. 
The benefit is that you re-use the same material easily.


License
-------

MIT License (see [LICENSE](LICENSE.md))
